export class IFeed{
    id?: string;
    title: string;
    author: string;
    date: Date;
    url: string;
    ext_id: string;
    hidden: boolean;
}