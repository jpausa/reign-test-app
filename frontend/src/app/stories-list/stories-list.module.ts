import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoriesListService } from './service/stories-list.service';
import { StoriesListComponent } from './components/stories-list.component';
import { MomentModule } from 'ngx-moment';

@NgModule({
  imports: [CommonModule, MomentModule],
  declarations: [StoriesListComponent],
  providers: [StoriesListService],
  exports: [StoriesListComponent],
})
export class StoriesListModule {}
