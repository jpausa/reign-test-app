import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { interval, Subscription } from 'rxjs';
import { StoriesListService } from '../service/stories-list.service';

@Component({
  selector: 'app-stories-list',
  templateUrl: './stories-list.component.html',
  styleUrls: ['./stories-list.component.css'],
})
export class StoriesListComponent implements OnInit {
  feeds: any[];
  serveroffline: boolean = false;
  updateSubscription: Subscription;

  constructor(private storieService: StoriesListService) {}

  ngOnInit(): void {
    this.getImports();
    //this can be done i a better way
    this.updateSubscription = interval(20000).subscribe((val) => {
      this.getImports();
    });
  }

  getImports() {
    this.storieService.getFeeds().subscribe(
      (res) => {
        if (res.length > 0) {
          this.serveroffline = false;
          this.setMomentLocale();
          this.feeds = res;
          localStorage.setItem('feeds', JSON.stringify(this.feeds));
        } else {
          this.serveroffline = true;
          this.setMomentLocale();
          this.feeds = JSON.parse(localStorage.getItem('feeds') || '[]');
        }
      },
      (err) => console.log(err)
    );
  }

  deleteFeed(feedId: string) {
    this.storieService.deleteFeed(feedId).subscribe(
      (res) => {
        this.getImports();
      },
      (err) => console.log(err)
    );
  }

  setMomentLocale() {
    moment.locale('en', {
      calendar: {
        lastDay: '[Yesterday]',
        sameDay: 'LT',
        nextDay: 'MMM D',
        lastWeek: 'MMM D',
        nextWeek: 'MMM D',
        sameElse: 'MMM D',
      },
    });
  }
}
