import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IFeed } from 'src/app/interfaces/feed.interface';

@Injectable({
  providedIn: 'root',
})
export class StoriesListService {
  BASE_URL: string = environment.backend;
  constructor(private http: HttpClient) {}

  getFeeds(): Observable<IFeed[]>{
    return this.http.get<IFeed[]>(`${this.BASE_URL}/feeds`);
  }

  deleteFeed(feedId: string): Observable<IFeed>{
    return this.http.put<any>(`${this.BASE_URL}/feeds/mark/${feedId}/true`, true );
  }

}
