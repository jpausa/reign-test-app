import { TestBed } from '@angular/core/testing';

import { StoriesListService } from './stories-list.service';

describe('StoriesListService', () => {
  let service: StoriesListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoriesListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
