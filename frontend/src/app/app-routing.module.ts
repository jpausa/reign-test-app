import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoriesListComponent } from './stories-list/components/stories-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'feeds',
    pathMatch: 'full',
  },
  {
    path: 'feeds',
    component: StoriesListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
