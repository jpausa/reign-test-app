## to run app 

in the root folder execute 

$  docker-compose up --build

backend server will be running in port:3000 
frontend server will be running in port:8000 

## to execute the backend project in dev mode 

$ export MONGO_HOST=localhost
$ export MONGO_PORT=27017
$ export MONGO_DB=NodeFeedsDB
$ cd backend
$ yarn install
$ yarn start:dev

to be able to run the project you need a mongodb server running in the port: 27017 
server will be running in port 3000

## to execute the frontend project in dev mode 

$ cd frontend
$ yarn install
$ yarn start

server will be running in port 4200
