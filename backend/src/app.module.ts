import { Module } from "@nestjs/common";
import { ScheduleModule } from "@nestjs/schedule";
import { MongoModule } from "./database/mongo.module";
import { ApiModule } from "./api/api.module";
import { FeedsModule } from "./api/feeds/feeds.module";

@Module({
  imports: [ScheduleModule.forRoot(), MongoModule, ApiModule, FeedsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
