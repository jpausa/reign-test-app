import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IFeed } from "../interfaces/feed.interface";

@Schema({ timestamps: true })
export class Feed {
  @Prop({ type: String})
  title: string;

  @Prop({ type: String })
  author: string;

  @Prop({ type: Date })
  date: Date;

  @Prop({ type: String })
  url: string;

  @Prop({ type: String })
  ext_id: string;

  @Prop({ type: Boolean })
  hidden: boolean;

}

export const FeedSchema = SchemaFactory.createForClass(Feed);

export class FeedFake {
  public async create(): Promise<any>{}
  public async save(): Promise<void> {}
  public async findByIdAndDelete(): Promise<any>{}
  public async deleteMany(): Promise<void>{}
  public async findOne(): Promise<void> {}
  public async findById(): Promise<void> {}

}