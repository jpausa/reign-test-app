import { Document } from "mongoose";

export interface IFeed extends Document {
  id: string;

  title: string;

  author: string;

  date: string;

  url: string;

  ext_id: string

  hidden?: boolean;
  
}
