import {
  HttpService,
  Injectable,
  OnApplicationBootstrap,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { IFeed } from "src/common/interfaces/feed.interface";
import { map } from "rxjs/operators";
import { Cron, CronExpression } from "@nestjs/schedule";

@Injectable()
export class FeedsService implements OnApplicationBootstrap {
  constructor(
    @InjectModel("Feeds") private feedModel: Model<IFeed>,
    private http: HttpService
  ) {}

  onApplicationBootstrap() {
    this.importFeeds();
  }

  async getFeeds(): Promise<IFeed[]> {
    return await this.feedModel.find();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async importFeeds() {
    console.log("Now importing stories");

    const rawfeeds = await this.http
      .get(
        "http://hn.algolia.com/api/v1/search_by_date?query=nodejs&tags=story"
      )
      .pipe(map((feeds) => feeds.data.hits))
      .toPromise();

    for (let index = 0; index < rawfeeds.length; index++) {
      const filter = { ext_id: rawfeeds[index].objectID };

      let newFeed = await this.feedModel.findOne(filter);

      if (!newFeed) {
        const { title, author, created_at: date, url, objectID: ext_id } = rawfeeds[index]
        await this.feedModel.create<IFeed>({ title, author, date, ext_id, hidden: false, url });
      }
    }
    return rawfeeds;
  }

  async markAsHidden(feedId: string, mark: boolean) {
    const feed = await this.feedModel.findByIdAndUpdate(feedId, {hidden:mark}, {new: true});
    return mark;
  }
}
