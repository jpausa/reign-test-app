import { Test, TestingModule } from "@nestjs/testing";
import { of } from 'rxjs';
import { FeedsService } from './feeds.service';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { IFeed } from '../../common/interfaces/feed.interface';
import { HttpService } from '@nestjs/common';

describe('FeedsService', () => {

    let feedModel = {
        find: jest.fn(),
        findOne: jest.fn(),
        save: jest.fn(),
        create: jest.fn()
    }

    let service: FeedsService;
    let model: Model<IFeed>
    let http 

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                FeedsService,
                { provide: getModelToken("Feeds"), useValue: feedModel },
                { provide: HttpService, useValue: { get: jest.fn() } },

            ]
        })
            .compile()

        service = module.get<FeedsService>(FeedsService)
        model = module.get<Model<IFeed>>(getModelToken("Feeds"))
        http = module.get<HttpService>(HttpService)

    });

    it('should be defined', () => {
        expect(service).toBeDefined();
        expect(model).toBeDefined();
    });

    it('should get feeds', async () => {
        await service.getFeeds()
        expect(model.find).toBeCalled()
    });

    it('should import all the stories from the external API to my DB', async () => {
        const feed = {
            data: {
                hits: [{
                    title: 'TITLE',
                    author: 'AUTHOR',
                    created_at: new Date(),
                    url: 'http://localhost',
                    objectID: 'OBJECTID',
                }]
            }
        }

        http.get.mockReturnValue(of(feed))

        await service.importFeeds()

        expect(http.get).toBeCalled()

        expect(model.findOne).toBeCalledTimes(1)
        expect(model.create).toBeCalledTimes(1)


    });



});