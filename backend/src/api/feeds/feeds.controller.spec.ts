import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { FeedsController } from './feeds.controller';
import { FeedsService } from './feeds.service';
import * as request from 'supertest';

describe('FeedsController', () => {
  let controller: FeedsController;
  let service = { importFeeds: jest.fn(), markAsHidden: jest.fn(), getFeeds: jest.fn() };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FeedsController],
      providers: [FeedsService],
    })
      .overrideProvider(FeedsService).useValue(service)
      .compile();

    controller = module.get<FeedsController>(FeedsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });


  it('import feed', async () => {
    await controller.importFeeds()
    expect(service.importFeeds).toHaveBeenCalled()
  });


  it('should mark as hidden a storie and return true', async () => {

    const id = 'ID'
    const value = true

    service.markAsHidden.mockReturnValue(value)
    const result = await controller.markFeed(id, value)

    expect(service.markAsHidden).toBeCalledWith(id, value)
    expect(result).toBe(value)
  });

  it('should return all the stories', async () => {
    const result = await controller.getFeeds()
    expect(service.getFeeds).toBeCalled()
  });

});


describe('Feed Endpoint', () => {
  let app: INestApplication;
  let service = { importFeeds: () => ['raw import'], markAsHidden: jest.fn(), getFeeds: () => ['feed'] };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FeedsService],
      controllers: [FeedsController]
    })
      .overrideProvider(FeedsService).useValue(service)
      .compile();

    app = module.createNestApplication();
    await app.init();
  });

  it(`/GET feeds`, () => {
    return request(app.getHttpServer())
      .get('/feeds')
      .expect(200)
      .expect(service.getFeeds());
  });


  it(`/POST import`, () => {
    return request(app.getHttpServer())
      .post('/feeds/import')
      .expect(201)
      .expect(service.importFeeds());
  });

  it(`/PUT mark value`, () => {
    service.markAsHidden.mockReturnValue(true)
    return request(app.getHttpServer())
      .put('/feeds/mark/id/true')
      .expect(200)
      .expect('true');
  });

  afterAll(async () => {
    await app.close();
  });

})
