import { Controller, Get, Post, Delete, Param, Put } from "@nestjs/common";
import { FeedsService } from "./feeds.service";

@Controller("feeds")
export class FeedsController {
  constructor(private feedService: FeedsService) {}

  @Get()
  async getFeeds() {
    return await this.feedService.getFeeds();
  }
 
  @Post("import")
  async importFeeds() {
    return await this.feedService.importFeeds();
  }

  @Put("mark/:id/:value")
  async markFeed(@Param("id") id: string, @Param("value") value: boolean) {
    return await this.feedService.markAsHidden(id, value);
  }
}
