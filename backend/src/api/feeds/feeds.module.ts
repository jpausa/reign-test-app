import { HttpModule, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { FeedSchema } from "src/common/schemas/feed.schema";
import { FeedsController } from "./feeds.controller";
import { FeedsService } from "./feeds.service";

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature(
      [{ name: "Feeds", schema: FeedSchema, collection: "feed" }],
      "NodeFeedsDB"
    ),
  ],
  controllers: [FeedsController],
  providers: [FeedsService],
  exports:[FeedsService]
})
export class FeedsModule {}
