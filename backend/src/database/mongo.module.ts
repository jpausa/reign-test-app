import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

const MONGO_HOST = process.env.MONGO_HOST;
const MONGO_PORT = process.env.MONGO_PORT;
const MONGO_DB = process.env.MONGO_DB;

const DB_PATH = ["mongodb://", MONGO_HOST, ":", MONGO_PORT, "/", MONGO_DB].join(
  ""
);

console.log(DB_PATH);

@Module({
  imports: [MongooseModule.forRoot(DB_PATH,  { connectionName: 'NodeFeedsDB' })],
})
export class MongoModule {}
